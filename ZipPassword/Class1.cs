﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZipPassword
{
    public class ZipPassword
    {
        /// <summary>
        /// Opens a password locked zip file and reads the content.
        /// Then it returns the username and password asked for in the credentials.
        /// This method uses the standard location and password of the zipfile
        /// </summary>
        /// <param name="_credentialName">The selection parameter for what username and password we are looking for</param>
        /// <param name="_userName">The requested username</param>
        /// <param name="_password">The requested password</param>
        public static void getPassword(String _credentialName, out String _userName, out String _password)
        {
            openZip(@"C:\data\dll\passwords\password.zip", "mellon", _credentialName, out _userName, out _password);
        }

        /// <summary>
        /// Opens a password locked zip file and reads the content.
        /// Then it returns the username and password asked for in the credentials.
        /// This method lets you set the location and password of the zip file.
        /// </summary>
        /// <param name="_zipPath">The location of the zip file</param>
        /// <param name="_zipPwd">the password of the zip file</param>
        /// <param name="_credentialName">The selection parameter for what username and password we are looking for</param>
        /// <param name="_userName">The requested username</param>
        /// <param name="_password">The requested password</param>
        public static void getPassword(String _zipPath, String _zipPwd, String _credentialName, out String _userName, out String _password)
        {
            openZip(_zipPath, _zipPwd, _credentialName, out _userName, out _password);
        }

        private static void openZip(String _zipPath, String _zipPwd, String _credentialName, out String _userName, out String _password)
        {
            using (ZipFile zip = ZipFile.Read(_zipPath))
            {
                MemoryStream stream = new MemoryStream();
                ZipEntry e = zip["Password.txt"];
                e.ExtractWithPassword(stream, _zipPwd);
                String[] myStrings = Encoding.UTF8.GetString(stream.ToArray()).Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                Dictionary<string, string> pwdList = new Dictionary<string, string>();
                foreach (string item in myStrings)
                {
                    var stringSplit = item.Split('|');
                    pwdList.Add(stringSplit[0], stringSplit[1]);
                }

                if (pwdList.ContainsKey(_credentialName))
                {
                    String[] cred = pwdList[_credentialName].Split(';');
                    _userName = cred[0];
                    _password = cred[1];
                }
                else
                {
                    _userName = null;
                    _password = null;
                }
            }
        }

    }
}
